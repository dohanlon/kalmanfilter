from seaborn import apionly as sns

import matplotlib.pyplot as plt

from matplotlib import rcParams
import matplotlib as mpl
mpl.use('Agg')

plt.style.use(['seaborn-whitegrid', 'seaborn-ticks'])
import matplotlib.ticker as plticker
rcParams['figure.figsize'] = 12, 8
rcParams['axes.facecolor'] = 'FFFFFF'
rcParams['savefig.facecolor'] = 'FFFFFF'
rcParams['figure.facecolor'] = 'FFFFFF'

rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'

rcParams['mathtext.fontset'] = 'cm'
rcParams['mathtext.rm'] = 'serif'

rcParams.update({'figure.autolayout': True})

import numpy as np
np.random.seed(42)

from pprint import pprint

import tensorflow as tf

from genKFTracks import genTracks

d = 1.0 # Distance between planes
sigma = 10E-2 # Resolution f planes
N = 5 # Number of planes
z = 0.1 # Thickness of absorber
x0 = 0.01 # Radiation length of absorber
theta0 = 10E-2 # Multiple scattering uncertainty (TODO: use formula)

F = tf.constant(np.array( [[1, d], [0, 1]] ), dtype = tf.float32)
G = tf.constant(np.array( [[1/sigma ** 2, 0], [0, 0]] ), dtype = tf.float32)
H = tf.constant(np.array( [[1, 0], [0, 0]] ), dtype = tf.float32)
Q = tf.constant(np.array( [[(theta0 ** 2 * z ** 2) / 3, (theta0 ** 2 * z) / 2],
               [(theta0 ** 2 * z) / 2,      theta0 ** 2]] ), dtype = tf.float32)

C0 = tf.constant(np.array( [[sigma ** 2, 0], [0, np.pi]] ), dtype = tf.float32)

def project(F, p, C, Q):

    print(p.shape)

    p_proj = F @ p
    C_proj = F @ C @ tf.transpose(F) + Q

    return p_proj, C_proj

def filter(p_proj, C_proj, H, G, m):

    inv_C_proj = tf.linalg.inv(C_proj)
    HG = tf.transpose(H) @ G

    C = tf.linalg.inv(inv_C_proj + HG @ H)

    p = inv_C_proj @ p_proj + HG @ m
    p = C @ p

    return p, C

def bkgTransport(C, F, C_proj):

    return C @ tf.transpose(F) @ tf.linalg.inv(C_proj)

def smooth(p_k1_smooth, p_k1_proj, C_k1_smooth, C_k1_proj, p_filtered, C_filtered, A):

    p_smooth = p_filtered + A @ (p_k1_smooth - p_k1_proj)
    C_smooth = C_filtered + A @ (C_k1_smooth - C_k1_proj) @ tf.transpose(A)

    return p_smooth, C_smooth

if __name__ == '__main__':

    hits, trueTracks = genTracks(nGen = 1)
    hits = hits[0]
    trueTracks = trueTracks[0]

    m0 = tf.constant(np.array([hits[0], 0.0]).reshape(-1, 1), dtype = tf.float32) # Measurement at 0, but can't measure theta?
    p0 = tf.constant(np.array([m0[0], 0.0]).reshape(-1, 1), dtype = tf.float32) # Guess?

    p_proj, C_proj = project(F, p0, C0, Q)

    p, C = filter(p_proj, C_proj, H, G, m0)

    projectedTrack = [p_proj]
    projectedCov = [C_proj]

    filteredTrack = [p]
    filteredCov = [C]

    for i in range(1, len(hits)):

        p_proj, C_proj = project(F, p, C, Q)

        m = tf.constant(np.array([hits[i], 0.0]).reshape(-1, 1), dtype = tf.float32)

        p, C = filter(p_proj, C_proj, H, G, m)

        filteredTrack.append(p)
        filteredCov.append(C)

        projectedTrack.append(p_proj)
        projectedCov.append(C_proj)

    smoothedTrack = [None for i in range(len(hits) - 1)] + [filteredTrack[-1]]
    smoothedCov = [None for i in range(len(hits) - 1)] + [filteredCov[-1]]

    reversedPlaneIndices = list(range(0, len(hits) - 1))
    reversedPlaneIndices.reverse()

    for i in reversedPlaneIndices:

        p_k1_proj, C_k1_proj = projectedTrack[i + 1], projectedCov[i + 1]
        p_filtered, C_filtered = filteredTrack[i], filteredCov[i]
        p_k1_smooth, C_k1_smooth = smoothedTrack[i + 1], smoothedCov[i + 1]

        A = bkgTransport(C_filtered, F, C_proj)

        p_smooth, C_smooth = smooth(p_k1_smooth, p_k1_proj, C_k1_smooth, C_k1_proj, p_filtered, C_filtered, A)

        smoothedTrack[i] = p_smooth
        smoothedCov[i] = C_smooth

    # Plot

    for i in range(1, N + 1):
        plt.plot([i, i], [-3, 3], color = 'k', linestyle = '--', alpha = 0.15)

    plt.plot(np.array(range(1, N + 1)), hits, 'x', color = 'k')
    plt.plot(np.array(range(1, N + 1)), [x[0] for x in filteredTrack], alpha = 0.5)
    plt.plot(np.array(range(1, N + 1)), [x[0] for x in smoothedTrack])
    plt.plot(np.array(range(0, N + 1)), trueTracks, ls = ':')

    plt.savefig('kfTracks.pdf')
