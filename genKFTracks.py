from seaborn import apionly as sns

import matplotlib.pyplot as plt

from matplotlib import rcParams
import matplotlib as mpl
mpl.use('Agg')

plt.style.use(['seaborn-whitegrid', 'seaborn-ticks'])
import matplotlib.ticker as plticker
rcParams['figure.figsize'] = 12, 8
rcParams['axes.facecolor'] = 'FFFFFF'
rcParams['savefig.facecolor'] = 'FFFFFF'
rcParams['figure.facecolor'] = 'FFFFFF'

rcParams['xtick.direction'] = 'in'
rcParams['ytick.direction'] = 'in'

rcParams['mathtext.fontset'] = 'cm'
rcParams['mathtext.rm'] = 'serif'

rcParams.update({'figure.autolayout': True})

import numpy as np
np.random.seed(41)

def genTracks(nGen = 10, truthOnly = False, plot = False):

    d = 1.0 # Distance between planes
    sigma = 10E-2 # Resolution of planes
    N = 5 # Number of planes
    z = 0.1 # Thickness of absorber
    x0 = 0.01 # Radiation length of absorber
    theta0 = 10E-2 # Multiple scattering uncertainty (TODO: use formula)

    # Absorber lengths add
    msDists = np.array([i * d for i in range(1, N + 1)])

    # But resulting MS uncertainties add in quadrature
    msErrors = np.array([np.sqrt(i) * theta0 for i  in range(1, N + 1)])

    initialThetaRange = [np.radians(-30), np.radians(30)]

    xRange = [N * d * np.tan(initialThetaRange[0]), N * d * np.tan(initialThetaRange[1])]
    bins = np.arange(xRange[0], xRange[1], sigma)

    tanThetas = np.tan(np.random.uniform(*initialThetaRange, nGen))
    # tanThetas = np.tan([0 for i in range(nGen)])
    trueHits = np.outer(tanThetas, d * np.array(range(1, N + 1)))

    plotTracks = np.hstack((np.zeros((nGen, 1)), trueHits)) # Project tracks back to origin @ -1

    msGauss = np.random.normal(np.zeros(N), msErrors, (nGen, N))

    hits = trueHits + msGauss

    hitMap = np.digitize(hits, bins)
    digiHits = bins[hitMap]

    if plot:

        for i in range(1, N + 1):
            plt.plot([i, i], [-3, 3], color = 'k', linestyle = '--', alpha = 0.15)

        plt.plot(np.array(range(0, N + 1)), plotTracks.T)
        plt.plot(np.array(range(1, N + 1)), digiHits.T, 'x', color = 'k')

        plt.ylim(xRange[0] - 0.1, xRange[1] + 0.1)
        plt.xlim(-0.1, N + 0.1)

        plt.savefig('hits.pdf')

    if not truthOnly:
        return digiHits, plotTracks
    else:
        return trueHits

if __name__ == '__main__':

    genTracks(nGen = 10, plot = True)
